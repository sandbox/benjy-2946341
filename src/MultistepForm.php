<?php

namespace Drupal\multistep_form_api;

/**
 * Configuration object to represent a multistep form.
 */
class MultistepForm {

  /**
   * The steps.
   *
   * @var array
   */
  protected $steps = [];

  /**
   * The current step.
   *
   * @var int
   */
  protected $currentStep = 0;

  /**
   * Allows jumping forward in the wizard.
   *
   * @var bool
   */
  protected $forwardJumpsAllowed;

  /**
   * MultistepForm constructor.
   *
   * @param int|null $defaultStep
   *   The default step if any.
   * @param bool $forwardJumpsAllowed
   *   TRUE to allow jumping forward otherwise FALSE.
   */
  public function __construct($defaultStep = NULL, $forwardJumpsAllowed = TRUE) {
    $this->forwardJumpsAllowed = $forwardJumpsAllowed;
    if (!is_null($defaultStep)) {
      $this->currentStep = $defaultStep;
    }
  }

  /**
   * Checks if forward jumps are allowed.
   *
   * @return bool
   *   TRUE if you can jump forward otherwise FALSE.
   */
  public function isJumpsForwardEnabled() {
    return $this->forwardJumpsAllowed;
  }

  /**
   * Moves the current step forward 1.
   */
  public function forward() {
    $this->currentStep++;
  }

  /**
   * Moves the current step back 1.
   */
  public function back() {
    $this->currentStep--;
  }

  /**
   * Gets the current step fields.
   *
   * @return array
   *   Gets the fields for the current step.
   */
  public function getCurrentStepFields() {
    return $this->steps[$this->getCurrentStepDelta()]['fields'];
  }

  /**
   * Gets the current step delta.
   *
   * @return int
   *   The current step delta.
   */
  public function getCurrentStepDelta() {
    if (count($this->steps) === 0) {
      throw new \InvalidArgumentException('You cannot interact with the multistep form until you have added a step');
    }

    return (int) $this->currentStep;
  }

  /**
   * Check is the step is the first step.
   *
   * @param int $step
   *   The step delta to check.
   *
   * @return bool
   *   TRUE if it's the first step otherwise FALSE.
   */
  public function isFirstStep($step = NULL) {
    $step = is_null($step) ? $this->getCurrentStepDelta() : $step;
    return $step === 0;
  }

  /**
   * Check is the step is the last step.
   *
   * @param int $step
   *   The step delta to check.
   *
   * @return bool
   *   TRUE if it's the last step otherwise FALSE.
   */
  public function isLastStep($step = NULL) {
    $step = is_null($step) ? $this->getCurrentStepDelta() : $step;
    return $step === count($this->steps) - 1;
  }

  /**
   * Check is the step has been visited.
   *
   * @param int $step
   *   The step delta to check.
   *
   * @return bool
   *   TRUE if it's visited otherwise FALSE.
   */
  public function isStepVisited($step) {
    return $step < $this->getCurrentStepDelta();
  }

  /**
   * Check is the step is active.
   *
   * @param int $step
   *   The step delta to check.
   *
   * @return bool
   *   TRUE if it's active otherwise FALSE.
   */
  public function isActiveStep($step) {
    return $step === $this->getCurrentStepDelta();
  }

  /**
   * Check is the step is disabled.
   *
   * @param int $step
   *   The step delta to check.
   *
   * @return bool
   *   TRUE if it's disabled otherwise FALSE.
   */
  public function isStepDisabled($step) {
    if ($this->forwardJumpsAllowed) {
      return FALSE;
    }
    return $step > $this->getCurrentStepDelta();
  }

  /**
   * Gets the steps.
   *
   * @return array
   *   An array of step definitions.
   */
  public function getSteps() {
    return $this->steps;
  }

  /**
   * Adds a new step.
   *
   * @param string $name
   *   The step name.
   * @param array $fields
   *   The step fields.
   *
   * @return $this
   *   The current object.
   */
  public function addStep($name, array $fields) {
    $this->steps[] = [
      'name' => $name,
      'fields' => $fields,
    ];
    return $this;
  }

}
