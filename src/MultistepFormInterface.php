<?php

namespace Drupal\multistep_form_api;

use Drupal\Core\Form\FormStateInterface;

/**
 * An interface to help setup multistep content entity forms.
 */
interface MultistepFormInterface {

  /**
   * Initialised the multistep form config.
   *
   * @return \Drupal\multistep_form_api\MultistepForm
   *   The multistep form configuration object.
   */
  public function initialiseMultistepConfig();

  /**
   * This is the traditional form() method for multistep forms.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   The form array.
   */
  public function getMultistepForm(array $form, FormStateInterface $formState);

}
