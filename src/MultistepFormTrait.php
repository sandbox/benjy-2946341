<?php

namespace Drupal\multistep_form_api;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;

/**
 * Provides default implementation for the multistep form.
 */
trait MultistepFormTrait {

  /**
   * The multistep form object.
   *
   * @var \Drupal\multistep_form_api\MultistepForm
   */
  protected $multistepConfig;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $multistepFormConfig = $this->getMultistepFormConfiguration();
    $form = parent::form($form, $form_state);
    $form = $this->getMultistepForm($form, $form_state);

    foreach (Element::children($form) as $delta) {
      if (!in_array($delta, $multistepFormConfig->getCurrentStepFields(), TRUE)) {
        $form[$delta]['#access'] = FALSE;
      }
    }

    // Render the progress bar down and inject it before the form.
    $progress = $this->getProgressBar();
    $progressHtml = \Drupal::service('renderer')->renderRoot($progress);
    $form['#prefix'] = isset($form['#prefix']) ? $progressHtml . $form['#prefix'] : $progressHtml;
    $form['#after_build'][] = '::afterBuildRestructureActionButtons';

    return $form;
  }

  /**
   * Add a wrapper on the actions after the form is built.
   */
  public function afterBuildRestructureActionButtons($element) {
    // Hide the submit button on new entities unless it's the last step.
    $actions = &$element['actions'];

    // Reshuffle the buttons in to two groups.
    $actions['#attributes']['class'][] = 'actions-grouped';
    $actions['left'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['actions-left']],
    ];
    $actions['right'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['actions-right']],
    ];

    foreach (['back', 'delete'] as $key) {
      if (isset($actions[$key])) {
        $actions['left'][$key] = $actions[$key];
        unset($actions[$key]);
      }
    }

    foreach (['next', 'submit'] as $key) {
      if (isset($actions[$key])) {
        $actions['right'][$key] = $actions[$key];
        unset($actions[$key]);
      }
    }

    return $element;
  }

  /**
   * Go forward form submit callabck.
   */
  public function submitFormNext($form, FormStateInterface $formState) {
    $formState->setRebuild();
    $this->getMultistepFormConfiguration()->forward();
  }

  /**
   * Go back form submit callabck.
   */
  public function submitFormBack($form, FormStateInterface $formState) {
    $formState->setRebuild();
    $this->getMultistepFormConfiguration()->back();
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $multistepFormConfig = $this->getMultistepFormConfiguration();
    $actions = parent::actions($form, $form_state);

    // Hide the submit button on new entities unless it's the last step.
    $actions['submit']['#access'] = !$this->entity->isNew() || $multistepFormConfig->isLastStep();

    $actions['submit']['#value'] = $this->t('Finished');
    $actions['submit']['#weight'] = 99;

    if (isset($actions['delete'])) {
      $actions['delete']['#attributes']['class'][] = 'button--link';
    }

    if (!$multistepFormConfig->isFirstStep()) {
      $actions['back'] = [
        '#type' => 'submit',
        '#value' => $this->t('Back'),
        '#weight' => -20,
        '#submit' => ['::submitFormBack'],
        '#attributes' => ['class' => ['button--plain']],
      ];
    }

    if (!$multistepFormConfig->isLastStep()) {
      $actions['next'] = [
        '#type' => 'submit',
        '#value' => $this->t('Next'),
        '#weight' => 100,
        '#attributes' => ['class' => ['button--plain', 'ml-20']],
        '#submit' => ['::submitForm', '::submitFormNext'],
      ];
    }

    return $actions;
  }

  /**
   * Gets the multistep form configuration.
   *
   * @return \Drupal\multistep_form_api\MultistepForm
   *   The multistep form configuration.
   */
  protected function getMultistepFormConfiguration() {
    if (!isset($this->multistepConfig)) {
      $this->multistepConfig = $this->initialiseMultistepConfig();
    }
    return $this->multistepConfig;
  }

  /**
   * Builds the progress bar for the form.
   *
   * @return array
   *   The progress bar renderable.
   */
  public function getProgressBar() {
    $config = $this->getMultistepFormConfiguration();
    $build = [
      '#type' => 'container',
      '#attributes' => ['class' => ['progress-wizard-wrapper']],
      'list' => [
        '#theme' => 'item_list',
        '#list_type' => 'ol',
        '#attributes' => [
          'class' => [
            $config->isJumpsForwardEnabled() ? 'jumps-forward-enabled' : 'jumps-forward-disabled',
            'progress-wizard',
            'clearfix',
            'inline',
            'progress-wizard-steps-' . count($config->getSteps()),
          ],
        ],
        '#items' => [],
      ],
    ];

    foreach ($config->getSteps() as $delta => $step) {
      $classes = [];
      if ($config->isFirstStep($delta)) {
        $classes[] = 'first';
      }
      if ($config->isStepVisited($delta)) {
        $classes[] = 'visited';
      }
      if ($config->isLastStep($delta)) {
        $classes[] = 'last';
      }
      if ($config->isActiveStep($delta)) {
        $classes[] = 'active';
      }

      $url = Url::fromRoute('<current>');
      if ($config->isStepDisabled($delta)) {
        $classes[] = 'disabled';
        $build['list']['#items'][] = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => new FormattableMarkup('<em>@name</em>', ['@name' => $step['name']]),
          '#attributes' => ['class' => $classes],
        ];
      }
      else {
        $build['list']['#items'][] = [
          '#type' => 'link',
          '#title' => new FormattableMarkup('<em>@name</em>', ['@name' => $step['name']]),
          '#url' => $url,
          '#attributes' => ['class' => $classes],
        ];
        $url->setOption('query', [
          'step' => $delta,
        ]);
      }

    }
    return $build;
  }

}
