<?php

namespace Drupal\Tests\multistep_form_api\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\KernelTests\KernelTestBase;
use Drupal\multistep_form_api\MultistepForm;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

/**
 * Test the multistep form trait.
 *
 * @group multistep_form_api
 */
class MultistepFormTraitTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'user',
    'node',
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installConfig(['system']);
    NodeType::create(['type' => 'article', 'name' => 'Article'])->save();
  }

  /**
   * Test the correct actions are generated.
   */
  public function testGenerateActions() {
    $config = new MultistepForm();
    $config->addStep('Step 1', []);
    $config->addStep('Step 2', []);

    $formObj = $this->getForm($config);
    $form = $formObj->buildForm([], new FormState());

    // New forms have a next button only and no save, back or delete.
    $this->assertTrue(isset($form['actions']['next']));
    $this->assertFalse(isset($form['actions']['back']));
    $this->assertFalse(isset($form['actions']['delete']));
    $this->assertFalse($form['actions']['submit']['#access']);

    // Set an existing entity.
    $node = Node::create(['title' => 'Node 1', 'type' => 'article']);
    $node->save();
    $formObj->setEntity($node);
    $form = $formObj->buildForm([], new FormState());

    // We now have access to the next, save and delete buttons.
    $this->assertTrue(isset($form['actions']['next']));
    $this->assertTrue($form['actions']['submit']['#access']);
    $this->assertTrue(isset($form['actions']['delete']));
    $this->assertFalse(isset($form['actions']['back']));

    // Move to the second step, now we should get a back button but no delete
    // button.
    $config->forward();
    $form = $formObj->buildForm([], new FormState());
    $this->assertFalse(isset($form['actions']['next']));
    $this->assertTrue($form['actions']['submit']['#access']);
    $this->assertTrue(isset($form['actions']['delete']));
    $this->assertTrue(isset($form['actions']['back']));
  }

  /**
   * Test generating the progress bar.
   */
  public function testGenerateProgressBar() {
    $config = new MultistepForm();
    $config->addStep('Step 1', []);
    $config->addStep('Step 2', []);
    $formObj = $this->getForm($config);

    $progressBar = $formObj->getProgressBar();
    $this->assertCount(2, $progressBar['list']['#items']);

    // First item is marked as first and active.
    $this->assertContains('first', $progressBar['list']['#items'][0]['#attributes']['class']);
    $this->assertContains('active', $progressBar['list']['#items'][0]['#attributes']['class']);

    // Last item is marked as last but not disabled because jump forwards are
    // allowed.
    $this->assertContains('last', $progressBar['list']['#items'][1]['#attributes']['class']);
    $this->assertNotContains('disabled', $progressBar['list']['#items'][1]['#attributes']['class']);

    // Disable jump forwards and ensure the items we've not visited are
    // disabled.
    $config = new MultistepForm(NULL, FALSE);
    $config->addStep('Step 1', []);
    $config->addStep('Step 2', []);

    $formObj = $this->getForm($config);
    $progressBar = $formObj->getProgressBar();

    $this->assertContains('disabled', $progressBar['list']['#items'][1]['#attributes']['class']);

    // Test with more steps after we've navigated forward.
    $config->addStep('Step 3', []);
    $config->addStep('Step 4', []);
    $config->forward();
    $config->forward();

    $formObj = $this->getForm($config);
    $progressBar = $formObj->getProgressBar();

    $this->assertContains('visited', $progressBar['list']['#items'][0]['#attributes']['class']);
    $this->assertContains('visited', $progressBar['list']['#items'][1]['#attributes']['class']);
    $this->assertContains('active', $progressBar['list']['#items'][2]['#attributes']['class']);
    $this->assertContains('disabled', $progressBar['list']['#items'][3]['#attributes']['class']);
  }

  /**
   * Gets the setup form.
   *
   * @param \Drupal\multistep_form_api\MultistepForm $config
   *   The form config.
   *
   * @return \Drupal\Tests\multistep_form_api\Kernel\MultistepTestForm
   *   The form.
   */
  protected function getForm(MultistepForm $config) {
    $formObj = MultistepTestForm::create($this->container);
    $formObj->setModuleHandler($this->container->get('module_handler'));
    $formObj->setEntityTypeManager($this->container->get('entity_type.manager'));
    $formObj->setEntity(Node::create(['type' => 'article']));
    $formObj->multistepFormConfig = $config;

    return $formObj;
  }

}
