<?php

namespace Drupal\Tests\multistep_form_api\Kernel;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\multistep_form_api\MultistepForm;
use Drupal\multistep_form_api\MultistepFormInterface;
use Drupal\multistep_form_api\MultistepFormTrait;

/**
 * Form for testing.
 */
class MultistepTestForm extends ContentEntityForm implements MultistepFormInterface {

  use MultistepFormTrait;

  /**
   * The form config.
   *
   * @var \Drupal\multistep_form_api\MultistepForm
   */
  public $multistepFormConfig;

  /**
   * The form array if required.
   *
   * @var array
   */
  public $form = [];

  /**
   * {@inheritdoc}
   */
  public function initialiseMultistepConfig() {
    return $this->multistepFormConfig ?: new MultistepForm();
  }

  /**
   * {@inheritdoc}
   */
  public function getMultistepForm(array $form, FormStateInterface $formState) {
    return $this->form;
  }

}
