<?php

namespace Drupal\Tests\multistep_form_api\Unit;

use Drupal\multistep_form_api\MultistepForm;
use Drupal\Tests\UnitTestCase;

/**
 * Unit test for the multistep form config object.
 *
 * @group multistep_form_api
 * @coversDefaultClass \Drupal\multistep_form_api\MultistepForm
 */
class MulistepFormTest extends UnitTestCase {

  /**
   * @covers ::__construct
   */
  public function testInitialiseDefaultStep() {
    // Defaults to 0 if we don't provide a default step.
    $multistep = (new MultistepForm())->addStep('Step 1', []);
    $this->assertEquals(0, $multistep->getCurrentStepDelta());

    // Otherwise, we can set a default.
    $multistep = (new MultistepForm(2))->addStep('Step 1', []);
    $this->assertEquals(2, $multistep->getCurrentStepDelta());
  }

  /**
   * Tests moving forward.
   *
   * @covers ::forward
   */
  public function testForward() {
    $multistep = (new MultistepForm())->addStep('Step 1', []);
    $this->assertEquals(0, $multistep->getCurrentStepDelta());

    $multistep->forward();
    $this->assertEquals(1, $multistep->getCurrentStepDelta());
  }

  /**
   * Tests moving back.
   *
   * @covers ::back
   */
  public function testBack() {
    $multistep = (new MultistepForm(1))->addStep('Step 1', []);
    $this->assertEquals(1, $multistep->getCurrentStepDelta());

    $multistep->back();
    $this->assertEquals(0, $multistep->getCurrentStepDelta());
  }

  /**
   * Tests checking first step.
   *
   * @covers ::isFirstStep
   */
  public function testIsFirstStep() {
    $multistep = (new MultistepForm())->addStep('Step 1', []);

    // We start on the first step.
    $this->assertTrue($multistep->isFirstStep());

    // Move forward and we're no longer on the first step.
    $multistep->forward();
    $this->assertFalse($multistep->isFirstStep());

    // We can provide a step override.
    $this->assertTrue($multistep->isFirstStep(0));
    $this->assertFalse($multistep->isFirstStep(1));
  }

  /**
   * Tests last step check.
   *
   * @covers ::isLastStep
   */
  public function testIsLastStep() {
    $multistep = new MultistepForm();
    $multistep->addStep('test step', []);

    // We have only 1 step so it is of course the last step.
    $this->assertTrue($multistep->isLastStep());

    // Add another step and it won't be the last step.
    $multistep->addStep('test step2', []);
    $this->assertFalse($multistep->isLastStep());

    // Move forward and we are on the last step again.
    $multistep->forward();
    $this->assertTrue($multistep->isLastStep());

    // Accepts an override.
    $this->assertTrue($multistep->isLastStep(1));
    $this->assertFalse($multistep->isLastStep(0));
  }

  /**
   * Tests visited check.
   *
   * @covers ::isStepVisited
   */
  public function testIsStepVisited() {
    $multistep = new MultistepForm();
    $multistep->addStep('test step', []);

    // We've not gone past the first step.
    $this->assertFalse($multistep->isStepVisited(0));

    $multistep->forward();
    $this->assertTrue($multistep->isStepVisited(0));
  }

  /**
   * Tests active check.
   *
   * @covers ::isActiveStep
   */
  public function testIsStepActive() {
    $multistep = new MultistepForm();
    $multistep->addStep('test step', []);

    $this->assertTrue($multistep->isActiveStep(0));
    $this->assertFalse($multistep->isActiveStep(1));
  }

  /**
   * Test if the step is disabled.
   *
   * @covers ::isStepDisabled
   */
  public function testIsStepDisabled() {
    // When forward jumps are allowed, steps are never disabled.
    $multistep = new MultistepForm(0, TRUE);
    $multistep->addStep('test step1', []);
    $multistep->addStep('test step2', []);

    $this->assertFalse($multistep->isStepDisabled(0));
    $this->assertFalse($multistep->isStepDisabled(1));

    // When forward jumps are disabled, you can only jump to a step in the past.
    $multistep = new MultistepForm(0, FALSE);
    $multistep->addStep('test step1', []);
    $multistep->addStep('test step2', []);
    $multistep->addStep('test step3', []);

    // Not disabled because it's the active step.
    $multistep->forward();
    $this->assertFalse($multistep->isStepDisabled(1));

    // Is disabled because it's a forward step.
    $this->assertTrue($multistep->isStepDisabled(2));
  }

}
